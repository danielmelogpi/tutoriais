Configuração da chave ssh
=====

Comumente os tutoriais de git fazem uso de sessões ssh para realizar os commits. Esse é um uso recomendado por alguns motivos:
-   Praticidade: adicione sua chave ssh à sessão e você não precisa se autenticar com a senha, como seria em um clone que usou https.
- Segurança: por não precisar da sua senha memorizada, evita-se que você a anote ou que adote uma senha simples e comum, como aquela coisa besta que protege sua conta bancária. Proteja seu arquivo de chave privada e lembra da senha dele. Duas camadas de segurança.

Se esse tutorial não for tão claro, o github também fornece ajuda nessa configuração [nesse link](http://git-scm.com/book/pt-br/v1/Git-no-Servidor-Gerando-Sua-Chave-P%C3%BAblica-SSH). O livro ProGit também tem uma página com essas instruções no item [4.3 Git no Servidor - Gerando Sua Chave Pública SSH](http://git-scm.com/book/pt-br/v1/Git-no-Servidor-Gerando-Sua-Chave-P%C3%BAblica-SSH).

### Criando as chaves ssh

Criar as chaves é muito trivial. Primeiro, deixemos a frescura e:
- No linux, abra seu terminal favorito
- No windows, como você é esperto, instalou o git e permitiu a instalação do Git Bash (trabalho numa máquina windows e essa coisinha me ajuda demais).

Vamos criar chaves assimétricas, o que na superfície significa que teremos duas chaves diferentes. Uma fica conosco, guardada em local seguro. A outra você fornece ao seu par na conexão, nesse caso, ao Gitlab.

No terminal, digite
```shell
$ ssh-add -l
2048 1e:c4:9b:15:84:a3:72:75:76:60:18:c5:aa:11:fb:83 daniel@earth-kingdom (RSA)
2048 ab:6a:a9:d4:3e:3e:72:be:da:f5:2f:ca:34:09:fd:36 daniel@earth-kingdom (RSA)
```
Isso lista as chaves que você já tem na sessão. Se você tiver como resposta qualquer coisa como "o agente não está rodando", execute

```shell
$ eval $(ssh-agent)
Agent pid 24793
```

Agora que você já sabe listar, vamos criar uma nova para o Gitlab. Eu recomendo ter uma para cada máquina com a qual você se conecta. Se uma dessas chaves "vazar" você pode facilmente negar acesso à quem a tiver.


O comando `ssh-keygen` é quem faz a mágica. Vamos fazer isso da maneira mais simples, mas ele tem algumas opções extras que você pode explorar depois.

O comando vai te pedir o lugar para a nova chave (digite o caminho completo), uma senha, e vai gerar um desenho aleatório sem importância no final.
- No linux, a convenção coloca a pasta em `/home/meuusuario/.ssh/meu-arquivo-chave` como o local preferível.
- No windows, utilize o equivalente, em `/c/Users/seuusuario/.ssh/meu-arquivo-chave` você pode indicar o local equivalente com `~/.ssh`. O Git Bash interpreta sua pasta em c:/Users/meuusuario como uma HOME unix. Só lembrando: para navegar usado o Git Bash, refira-se às suas partições como `/c/` em vez de `c:/`. Fora isso, é tudo muito parecido com o linux.


```shell
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/daniel/.ssh/id_rsa): /home/daniel/.ssh/tutorial-gitlab
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/daniel/.ssh/tutorial-gitlab.
Your public key has been saved in /home/daniel/.ssh/tutorial-gitlab.pub.
The key fingerprint is:
0c:eb:10:5c:be:45:0d:1f:4c:81:95:9d:b4:e9:c4:62 daniel@earth-kingdom

```

O comando `ssh-add -l` já deve listar sua chave dentro da sessão. Se não, adicione manualmente com `ssh-add /meu/arquivo/privado` e informe a senha.


Agora vamos até a pasta. Foi criado um arquivo sem extensão (sua chave privada) e um com a extensão .pub. Colete o conteudo da .pub e o configure no gitlab nas opções da sua conta relativas à ssh keys. Não retire nem adicione carateres (incluindo espaços e quebras de linha).

```shell
$  .ssh  pwd
/home/daniel/.ssh
$  .ssh  ls
github-key  github-key.pub  gitlab-home
gitlab-home.pub  known_hosts
tutorial-gitlab  tutorial-gitlab.pub
$  .ssh
$ cat   tutorial-gitlab.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCTuA/jJ8ch/WoY3RiQi02xS71ed2uitMnaEXS4ACyhuO7yqrxKhySKvPqzgVE570+A6ljdj/7UxOAaDaj93D0BV6kElNWngViYu0Lh0+SiRQeswZCzr4LcCQPMaz1AWGwqrxAn4m4xSqMKhC3KPE1l0M4jt9T1fL8gem+9K4T825VXMW91D/fTBhytSBqBKsAaJy2nRS7OObWnxNW3PmFuRn0n1QRAIVOvn2iwkT9UtJGCmNJwODlFPE4mavzGDt5qVH4fl712P9TXwiU9sm7ZTcnNnmOyjk2WK+J5iWp9ZUnFRa7KimLVqTxcnjA++Xqf4hCuU/bUCSQkeWmvsOon daniel@earth-kingdom
```

![Adicionando nova chave ao gitlab](https://gitlab.com/danielmelogpi/tutoriais/raw/master/docs/ferramentas/git/images/profile-ssh-keys-add-new.png)
![Nova chave adicioanda](https://gitlab.com/danielmelogpi/tutoriais/raw/master/docs/ferramentas/git/images/profile-ssh-keys-added-keys.png)


Agora você já pode fazer clones usando os repositórios endereçados no formato `git@gitlab.com:danielmelogpi/tutoriais.git` e, principalmente, fazer push para os repositórios. Mais simples que isso só dois desse. Lembre-se de guardar sua chave privada.
