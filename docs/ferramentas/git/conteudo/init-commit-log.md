Meu primeiro commit
=======

Se você já usa algum sistema de controle de versão essas coisas são parta da sua rotina e, para nossa alegria, os comandos são muito parecidos com os do svn. As coisas vão começar a se diferenciar quando e se você quiser explorar mais a ferramenta (o que eu seriamente recomendo. Sério.)

Vamos fazer um pequeno exercício: vamos iniciar um repositório completamente novo, versionar um arquivo simples, commitar algumas versões dele e observar alguns logs.

### Iniciando um repositório

A primeira coisa a se fazer é iniciar. Navegue até a pasta que servirá da base ou raiz do repositório e use o comando `git init`. Como o nome sugere, acabamos de criar um novo repositório.

```
git init
Initialized empty Git repository in /tmp/meutexto/.git/
```
Note que foi criada nessa pasta uma outra chamada .git. Aqui começa a mágica: tudo o que você salvar na história dos arquivos fica dentro dela. Você vai descobrir logo como é glorioso poder fazer branches, merges, diffs e voltar no tempo quando tudo é local.

Vamos ver como esta o histórico e o status nesse primeiro momento:
```
$ git log
fatal: bad default revision 'HEAD'
$ git status
No ramo master

Submissão inicial.

```

* Note que houve um "erro fatal". Isso acontece por que o repositório não tem história pra contar.
* HEAD é um nome especial no git que aponta para o commit sobre o qual se está trabalhando. Não há commit, *ergo*, não temos HEAD.
* O `git status` indica que existe conteúdo pra commitar: arquivo1.txt. Indica ainda que ele não foi versionado.

### Versionando
Por que um repositório se não para versionar alguma coisa? Criemos um arquivo com algum texto!

```
echo "Coisas para fazer amanhã:
* lavar o tênis
* procurar aquele torrent" > arquivo1.txt
```

Agora vamos adicionar esse arquivo ao versionamento e commitá-lo

```
git add arquivo1.txt
git commit -m "Adicionada lista de coisas pra fazer"

[master (root-commit) f8384e4] Adicionada lista de coisas pra fazer
1 file changed, 3 insertions(+)
create mode 100644 arquivo1.txt
```
Devo escrever alguma coisa sobre o "conceito das três árvores" no futuro. Por hora, use o `git add`


### Acessando a história
Se você observar o log agora, podemos ver o commit e mais alguns dados sobre ele. Use o `git log` e você terá algo mais ou menos assim:
```
commit f8384e42393926e531635fa3b762bed380726594
Author: Daniel Melo <danielmelogpi@gmail.com>
Date:   Wed Mar 4 23:59:02 2015 -0300

Adicionada lista de coisas pra fazer
````
Lá está seu nome, e-mail, hora e mensagem de commit. Note também o hash sha1 na primeira linha: ele é o identificador unico do commit. Você pode ter um diff do que mudou naquele commit com o comando `git show f8384`. Note que não é preciso todo o hash, mas somente uns 5 digitos para trabalhar.

Não se assuste. Diffs são feios mesmo e guis são bem vindas aqui. O gitlab, inclusive, oferece visualização do que cada commit modificou. Exemplo [aqui](https://gitlab.com/danielmelogpi/ad-agentes-inteligentes/commit/ad12598ef4879de248ed3e3edd704831046fbdf1).

````diff

git show f8384

commit f8384e42393926e531635fa3b762bed380726594
Author: Daniel Melo <danielmelogpi@gmail.com>
Date:   Wed Mar 4 23:59:02 2015 -0300

Adicionada lista de coisas pra fazer

diff --git a/arquivo1.txt b/arquivo1.txt
new file mode 100644
index 0000000..e974451
--- /dev/null
+++ b/arquivo1.txt
@@ -0,0 +1,3 @@
+Coisas para fazer amanhã:
+* lavar o tênis
+* terminar o trabalho de ad

````

Experimente editar o arquivo e comitar mais algumas vezes com o `git add` e o `git commit`. Use o `git status` entre seus experimentos para ver as mensagens de cada momento e se habituar.

Veja só: você já pode salvar versões dos seus arquivos com essa simples introdução das operações mais básicas. Vamos desvendar outras operações em outras paginas em breve e só tende a melhorar.
