Configuração mínima
=====
O comando base para as configurações no git é o `git config`. Você pode ver todas as opções já configuradas com `git config --list`.

Para realizar os commits você terá de informar muito pouco: seu nome e e-mail já são o suficiente. Para fazer isso, use o seguinte:

```
git config --global user.name "Rudolph Cascalho"
git config --global user.email "rudolph.cascalho@inf.ufg.br"
```

A flag `--global` indica que isso será o padrão a ser considerado nos repositórios que você criar. Existe também a opção `--local`, que é usada quando sua identidade for aplicada somente à um repositório, como no caso em que você queira, nesse projeto, usar o e-mail do inf, por exemplo. Recomendo a primeira nesse primeiro momento.
