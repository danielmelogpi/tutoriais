Tutoriais rápidos de git
=======
Início
=====

Esse é um grupo de pequenos manuais a serem expandidos gradualmente sobre o uso de git para versionamento de codigo fonte, documentação e afins na disciplina de Aplicações Distribuidas (SI-2015.1).

Como esse material não se extenderá demais, fica a recomendação dos 3 primeros capítulos do livro [ProGit](http://git-scm.com/book/pt-br/v1), aberto à comunidade e completamente traduzido.
Nesse outro, um [git prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html) e super resumido.

Esses tutoriais não serão extensivos e o autor inicial agradece contribuições, em especial com sugestões para os colegas sobre como resolver problemas que formos enfrentando individualmente.

Essas páginas foram escritas seguindo o padrão markdown. É absurdamente simples escrever texto formatado assim e você pode contribuir com seu editor de texto favorito.

Ao menos a princípio, assumo que você tem o git instalado na sua máquina, incluindo, no windows, a opção "Git Bash" ativa. Vou dar instruções sobre as operações usando o terminal/Git bash. É interessante aprendê-las assim antes, pois as GUIs nem sempre cobrem toda a riqueza de operações do git.

Se você já instalou, algumas coisas são do seu interesse nesse momento:
* [Configurar o git para usar meu nome e email](conteudo/git-config.md)
* [Iniciar um repositório, fazer alguns commits e olhar os logs](conteudo/init-commit-log.md)
* Criar uma branch nova para trabalhar em um recurso novo
* Fundir esse trabalho no seu trabalho inicial
* Resolver conflitos
* Voltar no tempo
* Clonar coisas

Quanto você entender esses primeiros conceitos (e pudermos escrever sobre eles aqui), é bacana conhecer mais algumas coisas
* Forkear um repositório, como o do projeto e trazer para a máquina local para trabalhar no codigo/conteudo.
* [Configurar chaves ssh (obvio que vamos aprender com o ssh)](conteudo/config-ssh.md)
* Subir suas alterações para o gitlab, no seu fork do repositório
* Integrando o trabalho de outros no seu repositorio local
* Fazer um pedido de merge request
